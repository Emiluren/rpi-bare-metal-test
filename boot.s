.section ".text.boot" // Make sure the linker puts this at the start of the kernel image

.global _start

_start:
        // Only run on main core
        mrs     x1, mpidr_el1
        and     x1, x1, #3
        cbz     x1, on_main_core
halt:
        // Not on main core, loop forever
        wfe
        b       halt

on_main_core:
        // Set stack below code
        ldr     x1, =_start
        mov     sp, x1

        // Clean BSS section
        ldr     x1, =__bss_start
        ldr     w2, =__bss_size
clean:  cbz     w1, main_start
        str     xzr, [x1], #8
        sub     w2, w2, #1
        cbnz    w2, clean

main_start:
        bl main
        b halt
